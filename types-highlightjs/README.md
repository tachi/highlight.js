This is a stub types definition for @types/highlight.js (https://github.com/highlightjs/highlight.js).

highlight.js provides its own type definitions, so you don't need @types/highlight.js installed!